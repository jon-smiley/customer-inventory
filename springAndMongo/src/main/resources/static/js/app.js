console.log("app.js");


$("#subBtn").on('click', function(){

    var url = "http://localhost:3000/customers";
    var firstName;
    var lastName;
    var data = {
        "id": "",
        "firstName": $("#firstN").val(),
        "lastName": $("#lastN").val()
    };

   console.log(data);
   fetch(url, {
       method: 'POST',
       body:  JSON.stringify(data),
       headers:{
           'Content-Type': 'application/json;charset=UTF-8'
       }

   });

});

$(".delete").on('click', function(){
   console.log($(this).attr('id'));

   var id = $(this).attr('id');
   var url = "http://localhost:3000/delete/" + id;

   fetch(url,{
       method: "DELETE"
   });

});

$("#search").on('click', function(){
   // console.log($("#searchN").val());

   var name = $("#searchN").val();
   var url = "http://localhost:3000/customers/" + name;

   fetch(url, {
       method: "GET"
   });
});

function refresh(){
    window.location.reload();
}
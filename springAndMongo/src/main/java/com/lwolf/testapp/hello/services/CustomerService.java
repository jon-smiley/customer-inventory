package com.lwolf.testapp.hello.services;

import com.lwolf.testapp.hello.models.Customer;
import com.lwolf.testapp.hello.repository.CustomerRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;


import java.util.List;
import java.util.UUID;

@Service
public class CustomerService {

    private static CustomerRepository customerRepository;

    @Autowired
    public CustomerService(CustomerRepository customerRepository) {
        this.customerRepository = customerRepository;
    }

    public Customer getCustomerById(String id) {
       return customerRepository.findAllById(id);
    }

    public Customer getCustomerByFirst(String firstName){
        return customerRepository.findAllByFirstName(firstName);
    }


    public List<Customer> getCustomers() {
        return customerRepository.findAll();
    }

    public Customer addCustomer(Customer customer) {
        if (customer.id != null) {
            customer.setFirstName(customer.firstName);
            customer.setLastName(customer.lastName);
            customer.setId(UUID.randomUUID().toString());
            return customerRepository.save(customer);
        }
        return null;
    }



    public static Customer deleteCustomer (String id){
        try{
            Customer cust = customerRepository.findAllById(id);
            if (cust != null)
                 customerRepository.delete(cust);
        }catch (Exception ex){
            System.out.println("cannot find " + id);
            System.out.println(ex);

        }

        return null;
    }



}

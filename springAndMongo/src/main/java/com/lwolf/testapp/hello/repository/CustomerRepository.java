package com.lwolf.testapp.hello.repository;

import com.lwolf.testapp.hello.models.Customer;
import org.springframework.data.mongodb.repository.MongoRepository;

import java.util.List;


public interface CustomerRepository extends MongoRepository<Customer, String> {
    Customer findAllById(String id);
    Customer findAllByFirstName(String firstName);
    void delete(Customer cust);



}

package com.lwolf.testapp.hello.controllers;

import com.lwolf.testapp.hello.models.Customer;
import com.lwolf.testapp.hello.services.CustomerService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;

@Controller
@RequestMapping
public class CustomerController{
    private final static String CUSTOMER = "/customers";
    private final static String CUSTOMER_BY_ID = "/customers/{id}";
    private final static String CUSTOMER_BY_FIRST = "/customers/{firstName}";
    private final static String CUSTOMER_REMOVE = "/delete/{id}";
    private final static String CUSTOMER_EDIT = "/delete";


    private static CustomerService customerService;

    @Autowired
    public CustomerController(CustomerService customerService) {
        this.customerService = customerService;
    }

    @RequestMapping(path = CUSTOMER, method = RequestMethod.GET)
    public <List>String getCustomer(Model model) {
        model.addAttribute("customers", customerService.getCustomers());
        return "customers";
    }


    @RequestMapping(path = CUSTOMER, method = RequestMethod.POST)
    public String addCustomer(@RequestBody Customer customer){
        customerService.addCustomer(customer);

        return "redirect:/customers";
    }

    @RequestMapping("/create")
    public String create(Model model) {
        return "create";
    }
//    ==============================================================================

//    @RequestMapping(path = CUSTOMER_BY_ID, method = RequestMethod.GET)
//    public Customer getCustomerById(@PathVariable String id){
//        return customerService.getCustomerById(id);
//    }

//    @RequestMapping(path = CUSTOMER_BY_ID, method = RequestMethod.GET)
//    public static String getCustomerById(Model model){
//        model.addAttribute("customers", customerService.getCustomers());
//        return "customers";
//    }

        @RequestMapping(path = CUSTOMER_BY_FIRST, method = RequestMethod.GET)
        public String customer(@PathVariable String firstName, Model model) {
            model.addAttribute("customers", customerService.getCustomerByFirst(firstName));
            return "customers";
        }

//        @RequestMapping(path = CUSTOMER_BY_ID, method = RequestMethod.GET)
//        public String customers(@PathVariable String id, Model model) {
//        model.addAttribute("customers", customerService.getCustomerById(id));
//        return "customers";
//        }


//    ===================================================================================
    @RequestMapping(path = CUSTOMER_EDIT, method = RequestMethod.GET)
    public <List>String editCustomer(Model model) {
        model.addAttribute("delete", customerService.getCustomers());
        return "delete";
    }

   @RequestMapping(path = CUSTOMER_REMOVE, method = RequestMethod.DELETE)
   public Customer deleteCustomer(@PathVariable String id){

       return CustomerService.deleteCustomer(id);
    }

    @RequestMapping("/delete")
    public String delete(Model model){
        return "delete";
    }

}
